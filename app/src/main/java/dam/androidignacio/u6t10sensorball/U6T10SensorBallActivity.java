package dam.androidignacio.u6t10sensorball;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.ContextCompat;

import android.content.Context;
import android.content.pm.ActivityInfo;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.os.Bundle;

public class U6T10SensorBallActivity extends AppCompatActivity {

    private SensorManager sensorManager;
    private BallView ballView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        // create and set new view for the activity
        ballView = new BallView(this, ContextCompat.getDrawable(this, R.drawable.space_background));
        setContentView(ballView);

        // get instance of system sensor manager
        sensorManager = (SensorManager) getSystemService(Context.SENSOR_SERVICE);

    }

    @Override
    protected void onPause() {
        sensorManager.unregisterListener(ballView);
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
        sensorManager.registerListener(ballView,
                                       sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER),
                                       SensorManager.SENSOR_DELAY_GAME);    // SENSOR_DALY_GAME TO GET A GOOD FREQUENCY
    }
}