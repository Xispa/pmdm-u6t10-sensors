package dam.androidignacio.u6t10sensorball;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.drawable.Drawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.media.Image;
import android.view.View;

import java.util.Random;

public class BallView extends View implements SensorEventListener {

    private Ball ball;
    private Paint pen;

    private int width, height;

    SoundManager soundManager;

    Bitmap bitmapBall;

    // Constructor
    public BallView(Context context, Drawable background) {
        super(context);

        setBackground(background);

        bitmapBall = BitmapFactory.decodeResource(context.getResources(), R.drawable.moon);

        // Ball
        ball = new Ball(bitmapBall.getWidth());

        // pen to paint the ball
        pen = new Paint();

        soundManager = new SoundManager();
        soundManager.initSoundManager(context);
        soundManager.addSound(1, R.raw.ball_bounce);
        soundManager.addSound(2, R.raw.ball_jump);
        soundManager.addSound(3, R.raw.ball_kitty);
        soundManager.addSound(4, R.raw.ball_twitch);
    }

    // Linear interpolation
    private double map(double value, double o1, double o2, double d1, double d2) {
        double result;
        double scale = (d2 -d1) / (o2 - o1);
        result = (value - o1) * scale + d1;
        return result;
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);

        // GET Width and Height: limits of the view
        width = w;
        height = h;

        // set initial position of the ball
        ball.x = w / 2;
        ball.y = 0;
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        // Draw the ball
        canvas.drawBitmap(bitmapBall, (float) ball.x, (float) ball.y, pen);

    }

    protected void updatePhisics() {

        // Detecting collision to make ball bounce
        boolean collision = false;

        // X
        if (ball.x + ball.diameter > width - 1) {
            ball.vx = -ball.vx / 2.0;
            ball.x = width - 1 - ball.diameter;
            collision = true;
        }
        if (ball.x - ball.diameter / 2 < 0) {
            ball.vx = -ball.vx / 2.0;
            ball.x = ball.diameter / 2;
            collision = true;
        }
        // Y
        if (ball.y + ball.diameter > height - 1) {
            ball.vy *= - 1;
            ball.y = height - 1 - ball.diameter;
            collision = true;
        }
        if (ball.y - ball.diameter / 2 < 0) {
            ball.vy *= -1;
            ball.y = ball.diameter / 2;
            collision = true;
        }
        // end detecting collision

        // play sound if there was any collision
        if (collision) {
            soundManager.playSound(new Random().nextInt(soundManager.getLength()) + 1);
        }

        // Update velocity ...
        ball.vy += ball.GRAVITY;
        ball.vx += ball.ax;

        // ... and set x,y position
        ball.x += ball.vx;
        ball.y += ball.vy;
    }

    // sensor callback methods
    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) { }

    @Override
    public void onSensorChanged(SensorEvent event) {

        if (event.sensor.getType() == Sensor.TYPE_ACCELEROMETER) {

            double value = event.values[0];     // get X accel value

            // if value<0 device moves to right
            // if value>0 device moves to left

            // adjust big/small values between -d and 5 at maximum
            if (value > 5)  value = 5;
            if (value < -5) value = -5;

            if (value < 0.1 && value > -0.1) {      // discard small accelerations
                ball.ax = 0.0;
            } else {
                // calculate the mapped acceleration in abs value
                ball.ax = map(Math.abs(value), 0.1, 5, 0.05, 0.5);
                // make ax negative if value was originally negative
                if (value < 0.0) ball.ax *= -1;
            }
            // adjust ax sign to show it later right on screen
            ball.ax *= -1;

            // update data of the ball
            updatePhisics();

            // force system to redraw view again
            invalidate();
        }
    }

}













