package dam.androidignacio.u6t10sensorball;

public class Ball {
    public final double GRAVITY = 1;
    public int diameter;
    public double ax;       // acceleration over X axis
    public double vx, vy;   // velocity
    public double x, y;     // position

    public Ball(int diameter) {
        this.diameter = diameter;
        vx = 0;
        vy = 1;
        x = y = 0;
        ax = 0;
    }
}