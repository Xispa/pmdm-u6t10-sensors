package dam.androidignacio.u6t10sensorball;

import android.content.Context;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.SoundPool;
import android.os.Build;
import android.util.Log;
import android.util.SparseIntArray;

public class SoundManager {

    private SoundPool mySoundPool;
    private final int MAX_SOUNDPOOL_STREAMS = 4;

    // private HashMap<Integer, Integer> mySoundPoolList;
    private SparseIntArray mySoundPoolList;
    private AudioManager audioManager;
    private Context context;
    private int soundsLoaded = 0;

    public void initSoundManager(Context theContext) {
        context = theContext;

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP) {
            // deprecated
            mySoundPool = new SoundPool(MAX_SOUNDPOOL_STREAMS, AudioManager.STREAM_MUSIC, 0);
        } else {
            AudioAttributes audioAttributes = new AudioAttributes.Builder()
                    .setUsage(AudioAttributes.USAGE_GAME)
                    .setContentType(AudioAttributes.CONTENT_TYPE_MUSIC)
                    .build();

            mySoundPool = new SoundPool.Builder().setAudioAttributes(audioAttributes)
                    .setMaxStreams(MAX_SOUNDPOOL_STREAMS)
                    .build();
        }

        // nou: listener
        mySoundPool.setOnLoadCompleteListener(new SoundPool.OnLoadCompleteListener() {
            @Override
            public void onLoadComplete(SoundPool soundPool, int sampleId, int status) {
                if (status == 0) {
                    soundsLoaded++;
                }
            }
        });

        mySoundPoolList = new SparseIntArray();
        audioManager = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
    }

    // method to add sounds to the pool list
    public void addSound(int index, int soundID) {
        mySoundPoolList.put(index, mySoundPool.load(context, soundID, 1));
    }

    // play methods
    public void playSound(int index) {
        if (soundsLoaded == mySoundPoolList.size()) {
            mySoundPool.play(mySoundPoolList.get(index), 0.5f, 0.5f, 1, 0, 1f);
            Log.i("SOUND", "playing");
        }
    }

    public int getLength() {
        return mySoundPoolList.size();
    }

}
